﻿<%@ Page Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="EmpManage.aspx.cs" Inherits="Ass1Homework.EmpManage" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main" >

        <div class="alert alert-danger" role="alert" id="alertBox" runat="server">

            <a href="#" class="close" data-dismiss="alert">&times</a>
            <asp:GridView ID="gvAlert" runat="server">
                <Columns>
                    <asp:TemplateField>
                            <ItemTemplate>
                            <asp:Label Text='<%# Eval("stroge") %>' runat="server" ForeColor="Red" ID="lblArStock"/>
                        </ItemTemplate>
                    </asp:TemplateField>   
                    <asp:TemplateField>
                            <ItemTemplate>
                            <asp:Label Text='<%# Eval("ItemName") %>' runat="server" ForeColor="Red" ID="lblArItem"/>
                        </ItemTemplate>
                    </asp:TemplateField>   
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblAlert" runat="server" Text=""/>
                </div>

    <div style="width: 1000px">
            <h1> Employee Management</h1>
        </div>
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <button type="button" class="btn btn-secondary" onclick="window.location.href='EmpManage.aspx'">Item Management</button>
                <button type="button" class="btn btn-secondary" onclick="window.location.href='IncOfEmp.aspx'">Income</button>
                   
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                       
                    <asp:TextBox ID="TextBoxShEmp" runat="server" Height="43px"></asp:TextBox>
                                       
                    <asp:Button ID="ButtonSearch" runat="server" OnClick="ButtonSearch_Click" Text="LOGIN" Height="43px" />
                                       
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblTotalDept" runat="server" Font-Bold="True" Height="43px" Width="200px"></asp:Label>
                                       
                </div>
                <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False"  ShowFooter="True"  DataKeyNames="ItemID"
                ShowHeaderWhenEmpty="True" PagerSettings-Position="TopAndBottom"
                OnRowCommand="gvItem_RowCommand" OnRowEditing="gvItem_RowEditing" OnRowCancelingEdit="gvItem_RowCancelingEdit" 
                OnRowUpdating="gvItem_RowUpdating" 
                
                BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" Width="1000px" ForeColor="Black" GridLines="Vertical" OnRowDeleting="gvItem_RowDeleting" >
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />

                <PagerSettings Position="TopAndBottom"></PagerSettings>

                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#808080" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                <Columns>
                    <asp:TemplateField HeaderText="ItemID">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("itemID") %>' runat="server" ID="txtItemID" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ItemName">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("ItemName") %>' runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtItemName" Text='<%# Eval("ItemName") %>' runat="server" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtitemNameFooter" runat="server"  />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Color">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("Color") %>' runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                        <asp:DropDownList ID="txtColor"  runat="server">
                            <asp:ListItem>YELLO</asp:ListItem>
                            <asp:ListItem>ORANGE</asp:ListItem>
                            <asp:ListItem>RED</asp:ListItem>
                            <asp:ListItem>PINK</asp:ListItem>
                            <asp:ListItem>BLUE</asp:ListItem>
                            <asp:ListItem>GREEN</asp:ListItem>
                            <asp:ListItem>BROWN</asp:ListItem>
                            <asp:ListItem>GRAY</asp:ListItem>
                            <asp:ListItem>BLACK</asp:ListItem>
                            <asp:ListItem>WHITE</asp:ListItem>
                        </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                        <asp:DropDownList ID="txtColorFooter" runat="server">
                            <asp:ListItem>YELLO</asp:ListItem>
                            <asp:ListItem>ORANGE</asp:ListItem>
                            <asp:ListItem>RED</asp:ListItem>
                            <asp:ListItem>PINK</asp:ListItem>
                            <asp:ListItem>BLUE</asp:ListItem>
                            <asp:ListItem>GREEN</asp:ListItem>
                            <asp:ListItem>BROWN</asp:ListItem>
                            <asp:ListItem>GRAY</asp:ListItem>
                            <asp:ListItem>BLACK</asp:ListItem>
                            <asp:ListItem>WHITE</asp:ListItem>
                        </asp:DropDownList>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Price">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("price") %>' runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPrice" Text='<%# Eval("price") %>' runat="server" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPriceFooter" runat="server" Width="60px" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Department">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("deptName") %>' runat="server"  />
                        </ItemTemplate>
                        <EditItemTemplate>
                             <asp:DropDownList ID="ddlEditDept" runat="server">      
                        </asp:DropDownList>  
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlDeptFooter" runat="server">
                        </asp:DropDownList>  
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Stock">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("Stroge") %>' runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtStock" Text='<%# Eval("Stroge") %>' runat="server" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtStockFooter" runat="server" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField >
                        <ItemTemplate>
                            <asp:ImageButton ID="editImb" ImageUrl="~/Images/edit.png" runat="server" CommandName="Edit" ToolTip="Edit" Width="20px" Height="20px"/>
                            <asp:ImageButton ID="delImb" ImageUrl="~/Images/delete.png" runat="server" CommandName="Delete" ToolTip="Delete" Width="20px" Height="20px"/>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="saveImb" ImageUrl="~/Images/save.png" runat="server" CommandName="Update" ToolTip="Update" Width="20px" Height="20px"/>
                            <asp:ImageButton ID="cancelImb" ImageUrl="~/Images/cancel.png" runat="server" CommandName="Cancel" ToolTip="Cancel" Width="20px" Height="20px"/>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="addNewImb" ImageUrl="~/Images/addnew.png" runat="server" CommandName="AddNew" ToolTip="AddNew" Width="20px" Height="20px"/>
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
                <asp:GridView ID="gvItem0" runat="server" AutoGenerateColumns="False"  ShowFooter="True"  DataKeyNames="ItemID"
                ShowHeaderWhenEmpty="True" PagerSettings-Position="TopAndBottom"
                OnRowCommand="gvItem_RowCommand" OnRowEditing="gvItem_RowEditing" OnRowCancelingEdit="gvItem_RowCancelingEdit" 
                OnRowUpdating="gvItem_RowUpdating" 
                
                BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" Width="1000px" ForeColor="Black" GridLines="Vertical" OnRowDeleting="gvItem_RowDeleting" >
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />

                <PagerSettings Position="TopAndBottom"></PagerSettings>

                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#808080" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                <Columns>
                    <asp:TemplateField HeaderText="ItemID">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("itemID") %>' runat="server" ID="txtItemID0" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ItemName">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("ItemName") %>' runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtItemName0" Text='<%# Eval("ItemName") %>' runat="server" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtitemNameFooter0" runat="server"  />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Color">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("Color") %>' runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                        <asp:DropDownList ID="txtColor0"  runat="server">
                            <asp:ListItem>YELLO</asp:ListItem>
                            <asp:ListItem>ORANGE</asp:ListItem>
                            <asp:ListItem>RED</asp:ListItem>
                            <asp:ListItem>PINK</asp:ListItem>
                            <asp:ListItem>BLUE</asp:ListItem>
                            <asp:ListItem>GREEN</asp:ListItem>
                            <asp:ListItem>BROWN</asp:ListItem>
                            <asp:ListItem>GRAY</asp:ListItem>
                            <asp:ListItem>BLACK</asp:ListItem>
                            <asp:ListItem>WHITE</asp:ListItem>
                        </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                        <asp:DropDownList ID="txtColorFooter0" runat="server">
                            <asp:ListItem>YELLO</asp:ListItem>
                            <asp:ListItem>ORANGE</asp:ListItem>
                            <asp:ListItem>RED</asp:ListItem>
                            <asp:ListItem>PINK</asp:ListItem>
                            <asp:ListItem>BLUE</asp:ListItem>
                            <asp:ListItem>GREEN</asp:ListItem>
                            <asp:ListItem>BROWN</asp:ListItem>
                            <asp:ListItem>GRAY</asp:ListItem>
                            <asp:ListItem>BLACK</asp:ListItem>
                            <asp:ListItem>WHITE</asp:ListItem>
                        </asp:DropDownList>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Price">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("price") %>' runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPrice0" Text='<%# Eval("price") %>' runat="server" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtPriceFooter0" runat="server" Width="60px" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Department">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("deptName") %>' runat="server"  />
                        </ItemTemplate>
                        <%--<EditItemTemplate>
                             <asp:DropDownList ID="ddlEditDept" runat="server">      
                        </asp:DropDownList>  
                        </EditItemTemplate>--%>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlDeptFooter0" runat="server">
                        </asp:DropDownList>  
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Stock">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("Stroge") %>' runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtStock0" Text='<%# Eval("Stroge") %>' runat="server" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtStockFooter0" runat="server" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField >
                        <ItemTemplate>
                            <asp:ImageButton ID="editImb0" ImageUrl="~/Images/edit.png" runat="server" CommandName="Edit" ToolTip="Edit" Width="20px" Height="20px"/>
                            <asp:ImageButton ID="delImb0" ImageUrl="~/Images/delete.png" runat="server" CommandName="Delete" ToolTip="Delete" Width="20px" Height="20px"/>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ID="saveImb0" ImageUrl="~/Images/save.png" runat="server" CommandName="Update" ToolTip="Update" Width="20px" Height="20px"/>
                            <asp:ImageButton ID="cancelImb0" ImageUrl="~/Images/cancel.png" runat="server" CommandName="Cancel" ToolTip="Cancel" Width="20px" Height="20px"/>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="addNewImb0" ImageUrl="~/Images/addnew.png" runat="server" CommandName="AddNew" ToolTip="AddNew" Width="20px" Height="20px"/>
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:GridView ID="gvSales" runat="server">
                <Columns>

                </Columns>
        </asp:GridView>
        <div class="alert alert-success" role="alert" ID="SuccessBox" runat="server">
            <a href="#" class="close" data-dismiss="alert">&times</a>
                <asp:Label ID="lblSuccessMessage" Text="" runat="server" ForeColor="Green" />
            </div>
        <div class="alert alert-danger" role="alert" ID="ErrorBox" runat="server">
            <a href="#" class="close" data-dismiss="alert">&times</a>
                <asp:Label ID="lblErrorMessage" Text="" runat="server" ForeColor="Red" />
            </div>

            
    </div>
</asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="head">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><title></title>
    <link rel="stylesheet" href="Content/bootstrap.min.css">
    <link rel="stylesheet" href="Content/main.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="Scripts/jquery-3.0.0.slim.min.js"></script>
    <script src="Scripts/umd/popper.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/main.js"></script>
    <style type="text/css">
        .btn-group-append {
            width: 874px;
        }
    </style>
</asp:Content>


