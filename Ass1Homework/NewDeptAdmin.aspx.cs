﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace Ass1Homework
{
    public partial class NewDeptAdmin : System.Web.UI.Page
    {
        string connectionString = @"Data Source=DESKTOP-PJ2H1R3\PUPATSQL;Initial Catalog=ex1;Integrated Security=True";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateGridview();
                gvDepartment0.Visible = false;
                SuccessBox.Visible = false;
                ErrorBox.Visible = false;
            }

        }


        void PopulateGridview()
        {
            DataTable dtbl = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT * FROM Department", sqlCon);
                sqlDa.Fill(dtbl);
            }
            if (dtbl.Rows.Count > 0)
            {
                
                gvDepartment.DataSource = dtbl;
                gvDepartment.DataBind();
                gvDepartment0.DataSource = dtbl;
                gvDepartment0.DataBind();
            }
            else
            {
                dtbl.Rows.Add(dtbl.NewRow());
                gvDepartment.DataSource = dtbl;
                gvDepartment.DataBind();
                gvDepartment.Rows[0].Cells.Clear();
                gvDepartment.Rows[0].Cells.Add(new TableCell());
                gvDepartment.Rows[0].Cells[0].ColumnSpan = dtbl.Columns.Count;
                gvDepartment.Rows[0].Cells[0].Text = "No Data Found ..!";
                gvDepartment.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            }
        }


        protected void gvDepartment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("AddNew"))
                {
                    using (SqlConnection sqlCon = new SqlConnection(connectionString))
                    {

                        sqlCon.Open();
                        string generateID = Generate();
                        int idChangeType = Convert.ToInt32(generateID);
                        idChangeType += 1;

                        string query = "INSERT INTO Department (deptID,deptName) VALUES (@deptID,@deptName)";
                        SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                        sqlCmd.Parameters.AddWithValue("@deptName", (gvDepartment.FooterRow.FindControl("txtDeptFooter") as TextBox).Text.Trim());
                        sqlCmd.Parameters.AddWithValue("@deptID", "DEPT0"+idChangeType.ToString());
                        sqlCmd.ExecuteNonQuery();
                        PopulateGridview();
                        lblSuccessMessage.Text = "New Record Added";
                        lblErrorMessage.Text = "";
                        SuccessBox.Visible = true;

                    }
                }
            }
            catch (Exception ex)
            {

                lblSuccessMessage.Text = "";
                lblErrorMessage.Text = ex.Message;
                ErrorBox.Visible = true;
            }
        }

        protected void gvDepartment_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvDepartment.EditIndex = e.NewEditIndex;
            PopulateGridview();
        }

        protected void gvDepartment_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

            gvDepartment.EditIndex = -1;
            PopulateGridview();
        }



        protected void gvDepartment_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {

                DataTable dtbl = new DataTable();
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();

                    

                    string query = "UPDATE Department SET DeptName=@Name WHERE DeptID = @id";
                    SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                    sqlCmd.Parameters.AddWithValue("@Name", (gvDepartment.Rows[e.RowIndex].FindControl("txtDeptName") as TextBox).Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@id", (gvDepartment.Rows[e.RowIndex].FindControl("deptID") as Label).Text.Trim());
                    sqlCmd.ExecuteNonQuery();
                    gvDepartment.EditIndex = -1;
                    lblSuccessMessage.Text = "Selected Record Update";
                    lblErrorMessage.Text = "";
                    PopulateGridview();
                }

            }
            catch (Exception ex)
            {

                lblSuccessMessage.Text = "";
                lblErrorMessage.Text = ex.Message;
                ErrorBox.Visible = true;
            }
        }

        protected void gvDepartment_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                DataTable dtbl = new DataTable();
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();

                    string query0 = "DELETE FROM Employee WHERE DeptID = @Dept";
                    SqlCommand sqlCmd0 = new SqlCommand(query0, sqlCon);
                    sqlCmd0.Parameters.AddWithValue("@Dept", (gvDepartment.Rows[e.RowIndex].FindControl("deptID") as Label).Text.Trim());
                    sqlCmd0.ExecuteNonQuery();


                    string querySales = "DELETE FROM Sales WHERE deptID = @idDept";
                    SqlCommand sqlCmdSales = new SqlCommand(querySales, sqlCon);
                    sqlCmdSales.Parameters.AddWithValue("@idDept", (gvDepartment.Rows[e.RowIndex].FindControl("deptID") as Label).Text.Trim());
                    sqlCmdSales.ExecuteNonQuery();


                    SqlDataAdapter da;
                    da  = new SqlDataAdapter("SELECT itemID FROM Sales WHERE deptID = @idDeptItem", sqlCon);
                    da.SelectCommand.Parameters.AddWithValue("@idDeptItem", (gvDepartment.Rows[e.RowIndex].FindControl("deptID") as Label).Text.Trim());
                    da.Fill(dtbl);


                    string query = "DELETE FROM Department WHERE DeptID = @id";
                    SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                    sqlCmd.Parameters.AddWithValue("@id", (gvDepartment.Rows[e.RowIndex].FindControl("deptID") as Label).Text.Trim());
                    sqlCmd.ExecuteNonQuery();
                    PopulateGridview();
                    lblSuccessMessage.Text = "Selected Record Deleted";
                    lblErrorMessage.Text = "";
                    SuccessBox.Visible = true;

                    
                }

            }
            catch (Exception ex)
            {

                lblSuccessMessage.Text = "";
                lblErrorMessage.Text = ex.Message;
                ErrorBox.Visible = true;
            }

        }

        private string Generate()
        {
            DataTable dtbl = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                string query = "SELECT DeptID FROM Department ORDER BY DeptID";
                SqlDataAdapter sqlDa = new SqlDataAdapter(query, sqlCon);
                sqlDa.Fill(dtbl);


                if (dtbl.Rows.Count <= 0)
                {
                    return "0";
                }
                else
                {
                    return (dtbl.Rows.Count + 1).ToString();
                }


            }
        }


    }


}
