﻿<%@ Page Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="NewDeptAdmin.aspx.cs" Inherits="Ass1Homework.NewDeptAdmin" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main" >    
    <div style="width: 847px">
            <h1> Admin Management</h1>
        </div>
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <button type="button" class="btn btn-secondary" onclick="window.location.href='EmpAdminManage.aspx'" >Department Management</button>
                
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="NewDeptAdmin.aspx">New Department</a>
                    <a class="dropdown-item" href="IncDeptAdmin.aspx">Income</a>
                    </div>
                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Department
                 </button>
                </div>
                <asp:GridView ID="gvDepartment" runat="server" AutoGenerateColumns="False"  ShowFooter="True" DataKeyNames="DeptID"      
                ShowHeaderWhenEmpty="True" PagerSettings-Position="TopAndBottom" 
                OnRowCommand="gvDepartment_RowCommand" OnRowEditing="gvDepartment_RowEditing" OnRowCancelingEdit="gvDepartment_RowCancelingEdit" 
                OnRowUpdating="gvDepartment_RowUpdating" OnRowDeleting="gvDepartment_RowDeleting"
                BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" Width="373px" ForeColor="Black" GridLines="Vertical" >
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />

<PagerSettings Position="TopAndBottom"></PagerSettings>

                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#808080" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                <Columns>
                    <asp:TemplateField HeaderText="Department">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("deptID") %>' runat="server" ID="deptID"/>
                        </ItemTemplate>
                        </asp:TemplateField>

                    <asp:TemplateField HeaderText="Department">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("deptName") %>' runat="server" ID="txtDept"/>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDeptName" Text='<%# Eval("deptName") %>' runat="server" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtDeptFooter" runat="server"  />
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ImageUrl="~/Images/edit.png" runat="server" CommandName="Edit" ToolTip="Edit" Width="20px" Height="20px"/>
                            <asp:ImageButton ImageUrl="~/Images/delete.png" runat="server" CommandName="Delete" ToolTip="Delete" Width="20px" Height="20px"/>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:ImageButton ImageUrl="~/Images/save.png" runat="server" CommandName="Update" ToolTip="Update" Width="20px" Height="20px"/>
                            <asp:ImageButton ImageUrl="~/Images/cancel.png" runat="server" CommandName="Cancel" ToolTip="Cancel" Width="20px" Height="20px"/>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ImageUrl="~/Images/addnew.png" runat="server" CommandName="AddNew" ToolTip="AddNew" Width="20px" Height="20px"/>
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        
                <asp:GridView ID="gvDepartment0" runat="server" AutoGenerateColumns="False"  ShowFooter="True" DataKeyNames="DeptID"      
                ShowHeaderWhenEmpty="True" PagerSettings-Position="TopAndBottom" 
                OnRowCommand="gvDepartment_RowCommand" OnRowEditing="gvDepartment_RowEditing" OnRowCancelingEdit="gvDepartment_RowCancelingEdit" 
                OnRowUpdating="gvDepartment_RowUpdating" OnRowDeleting="gvDepartment_RowDeleting"
                BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" Width="373px" ForeColor="Black" GridLines="Vertical" >
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />

<PagerSettings Position="TopAndBottom"></PagerSettings>

                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#808080" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                <Columns>
                    <asp:TemplateField HeaderText="Department">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("deptName") %>' runat="server" ID="txtDept0"/>
                        </ItemTemplate>
                    </asp:TemplateField>               
                </Columns>
            </asp:GridView>

        
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OnlineShopConnectionString %>" SelectCommand="SELECT * FROM [Department]"></asp:SqlDataSource>
            

            <div class="alert alert-success" role="alert" ID="SuccessBox" runat="server">
            <a href="#" class="close" data-dismiss="alert">&times</a>
                <asp:Label ID="lblSuccessMessage" Text="" runat="server" ForeColor="Green" />
            </div>
        <div class="alert alert-danger" role="alert" ID="ErrorBox" runat="server">
            <a href="#" class="close" data-dismiss="alert">&times</a>
                <asp:Label ID="lblErrorMessage" Text="" runat="server" ForeColor="Red" />
            </div>
    </div>
</asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="head">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><title></title>
    <link rel="stylesheet" href="Content/bootstrap.min.css">
    <link rel="stylesheet" href="Content/main.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="Scripts/jquery-3.0.0.slim.min.js"></script>
    <script src="Scripts/umd/popper.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/main.js"></script>
    <style type="text/css">
        .btn-group-append {
            width: 874px;
        }
    </style>
</asp:Content>

