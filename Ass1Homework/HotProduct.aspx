﻿<%@ Page Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="HotProduct.aspx.cs" Inherits="Ass1Homework.IncOfEmp" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main" >
  
    <div style="width: 847px">
            <h1> Employee Management</h1>
        </div>
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <button type="button" class="btn btn-secondary" onclick="window.location.href='EmpManage.aspx'" >Employee Management</button>
                    <button type="button" class="btn btn-secondary" onclick="window.location.href='HotProduct.aspx'" >HOT Product</button>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    
                </div>
        
                <div>
                    <asp:Label ID="lblHotColor" Text="" runat="server" Font-Bold="true" />
                </div>
                <asp:GridView ID="gvSales" runat="server" AutoGenerateColumns="False"  ShowFooter="True"  FooterStyle-BackColor="#808080" DataKeyNames="ItemName"      
                ShowHeaderWhenEmpty="True" PagerSettings-Position="TopAndBottom" 
                BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" Width="373px" ForeColor="Black" GridLines="Vertical" AllowSorting="true"  >
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />

                <PagerSettings Position="TopAndBottom"></PagerSettings>

                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#808080" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                <Columns>


                    <asp:TemplateField HeaderText="Item Name">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("itemName") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>

            <div class="alert alert-success" role="alert" ID="SuccessBox" runat="server">
            <a href="#" class="close" data-dismiss="alert">&times</a>
                <asp:Label ID="lblSuccessMessage" Text="" runat="server" ForeColor="Green" />
            </div>
        <div class="alert alert-danger" role="alert" ID="ErrorBox" runat="server">
            <a href="#" class="close" data-dismiss="alert">&times</a>
                <asp:Label ID="lblErrorMessage" Text="" runat="server" ForeColor="Red" />
            </div>
    </div>
</asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="head">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><title></title>
    <link rel="stylesheet" href="Content/bootstrap.min.css">
    <link rel="stylesheet" href="Content/main.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="Scripts/jquery-3.0.0.slim.min.js"></script>
    <script src="Scripts/umd/popper.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/main.js"></script>
    <style type="text/css">
        .btn-group-append {
            width: 874px;
        }
    </style>
</asp:Content>

