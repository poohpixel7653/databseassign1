﻿<%@ Page Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Ass1Homework.Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main">    
    <div>
            <h1> Search here</h1>
        </div>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search this blog" runat="server" id="SearchBox">
            <div class="input-group-append">
                <asp:Button ID="Search" runat="server" type="submit" class="btn btn-primary" Text ='Search' OnClick="Search_Click" />
            </div>
          </div>
    </div>
    <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <asp:Label ID="Label1" runat="server" Text="Label" Visible="false"></asp:Label>
                    </div>
                <div class="col-sm">
                   <asp:Label ID="Label2" runat="server" Text="Label" Visible="false"></asp:Label>
                </div>
                <div class="col-sm">
                   <asp:Label ID="Label3" runat="server" Text="Label" Visible="false"></asp:Label>
                </div>
                <div class="col-sm">
                   <asp:Label ID="Label4" runat="server" Text="Label" Visible="false"></asp:Label>
                </div>
              </div>
            </div>
</asp:Content>
