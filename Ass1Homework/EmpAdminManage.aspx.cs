﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace Ass1Homework
{
    public partial class EmpAdminManage : System.Web.UI.Page
    {
        string connectionString = @"Data Source=DESKTOP-PJ2H1R3\PUPATSQL;Initial Catalog=ex1;Integrated Security=True";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateGridview();
                SuccessBox.Visible = false;
                ErrorBox.Visible = false;
            }

        }


        void PopulateGridview()
        {
            DataTable dtblEmp = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                //SELECT item.itemID,item.itemName,item.color,item.stroge,Sales.price,Department.deptName FROM Item JOIN Sales ON item.itemID=Sales.itemID JOIN Department ON Sales.deptID=Department.deptID
                sqlCon.Open();
                SqlDataAdapter sqlDaEmp = new SqlDataAdapter("SELECT a.employeeID,a.nameEmployee,a.sex,a.age,a.salary,b.deptName FROM Employee AS a JOIN Department AS b ON a.deptID=b.deptID", sqlCon);
                sqlDaEmp.Fill(dtblEmp);
            }
            if (dtblEmp.Rows.Count > 0)
            {
                gvEmployee.DataSource = dtblEmp;
                gvEmployee.DataBind();
            }
            else
            {
                dtblEmp.Rows.Add(dtblEmp.NewRow());
                gvEmployee.DataSource = dtblEmp;
                gvEmployee.DataBind();
                gvEmployee.Rows[0].Cells.Clear();
                gvEmployee.Rows[0].Cells.Add(new TableCell());
                gvEmployee.Rows[0].Cells[0].ColumnSpan = dtblEmp.Columns.Count;
                gvEmployee.Rows[0].Cells[0].Text = "No Data Found ..!";
                gvEmployee.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            }
        }


        protected void gvEmployee_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("AddNew"))
                {
                    DataTable dtbl = new DataTable();
                    using (SqlConnection sqlCon = new SqlConnection(connectionString))
                    {
                        sqlCon.Open();
                        string generateID = Generate();
                        int idChangeType = Convert.ToInt32(generateID);
                        idChangeType += 1;

                        string query = "INSERT INTO Employee (employeeID,NameEmployee,Sex,Age,Salary,DeptID) VALUES (@employeeID,@Name,@Sex,@Age,@Salary,@Dept)";
                        SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                        sqlCmd.Parameters.AddWithValue("@EmployeeID", "EMP"+idChangeType.ToString());
                        sqlCmd.Parameters.AddWithValue("@Name", (gvEmployee.FooterRow.FindControl("txtNameFooter") as TextBox).Text.Trim());
                        sqlCmd.Parameters.AddWithValue("@Sex", (gvEmployee.FooterRow.FindControl("txtSexFooter") as DropDownList).SelectedValue);
                        sqlCmd.Parameters.AddWithValue("@Age", (gvEmployee.FooterRow.FindControl("txtAgeFooter") as TextBox).Text.Trim());
                        sqlCmd.Parameters.AddWithValue("@Salary", (gvEmployee.FooterRow.FindControl("txtSalaryFooter") as TextBox).Text.Trim());
                        sqlCmd.Parameters.AddWithValue("@Dept", (gvEmployee.FooterRow.FindControl("ddlDeptFooter") as DropDownList).Text.Trim());
                        sqlCmd.ExecuteNonQuery();
                        PopulateGridview();
                        lblSuccessMessage.Text = "New Record Added";
                        lblErrorMessage.Text = "";
                        SuccessBox.Visible = true;

                    }
                }
            }
            catch (Exception ex)
            {

                lblSuccessMessage.Text = "";
                lblErrorMessage.Text = ex.Message;
                ErrorBox.Visible = true;
            }
        }

        protected void gvEmployee_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvEmployee.EditIndex = e.NewEditIndex;
            PopulateGridview();
        }

        protected void gvEmployee_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvEmployee.EditIndex = -1;
            PopulateGridview();
        }

        protected void gvEmployee_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();

                    

                    string query = "UPDATE Employee SET nameEmployee=@Name,Sex=@Sex,Age=@Age,Salary=@Salary,DeptID=@Dept WHERE employeeID = @id";
                    SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                    sqlCmd.Parameters.AddWithValue("@Name", (gvEmployee.Rows[e.RowIndex].FindControl("txtName") as TextBox).Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Sex", (gvEmployee.Rows[e.RowIndex].FindControl("txtSex") as DropDownList).SelectedValue);
                    sqlCmd.Parameters.AddWithValue("@Age", (gvEmployee.Rows[e.RowIndex].FindControl("txtAge") as TextBox).Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Salary", (gvEmployee.Rows[e.RowIndex].FindControl("txtSalary") as TextBox).Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Dept", (gvEmployee.Rows[e.RowIndex].FindControl("ddlEditDept") as DropDownList).SelectedValue);
                    sqlCmd.Parameters.AddWithValue("@id", (gvEmployee.Rows[e.RowIndex].FindControl("EmpID") as Label).Text.Trim());
                    sqlCmd.ExecuteNonQuery();
                    gvEmployee.EditIndex = -1;
                    PopulateGridview();
                    lblSuccessMessage.Text = "Selected Record Update";
                    lblErrorMessage.Text = "";
                    SuccessBox.Visible = true;
                }

            }
            catch (Exception ex)
            {

                lblSuccessMessage.Text = "";
                lblErrorMessage.Text = ex.Message;
                ErrorBox.Visible = true;
            }
        }


        protected void gvEmployee_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();
                    string query = "DELETE FROM Employee WHERE employeeID = @id";
                    SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                    sqlCmd.Parameters.AddWithValue("@id", (gvEmployee.Rows[e.RowIndex].FindControl("EmpID") as Label).Text.Trim());
                    sqlCmd.ExecuteNonQuery();
                    PopulateGridview();
                    lblSuccessMessage.Text = "Selected Record Deleted";
                    lblErrorMessage.Text = "";
                    SuccessBox.Visible = true;
                }

            }
            catch (Exception ex)
            {

                lblSuccessMessage.Text = "";
                lblErrorMessage.Text = ex.Message;
                ErrorBox.Visible = true;
            }
        }

        private string Generate()
        {
            DataTable dtbl = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                string query = "SELECT employeeID FROM Employee ORDER BY employeeID";
                SqlDataAdapter sqlDa = new SqlDataAdapter(query, sqlCon);
                sqlDa.Fill(dtbl);


                if (dtbl.Rows.Count <= 0)
                {
                    return "0";
                }
                else
                {
                    return (dtbl.Rows.Count + 1).ToString();
                }


            }
        }



        protected void gvEmployee_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            DataTable dtbl = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                string query = "SELECT * FROM Department";
                SqlDataAdapter sqlDa = new SqlDataAdapter(query, sqlCon);
                sqlDa.Fill(dtbl);

                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    //Find the DropDownList in the Row
                    DropDownList ddlDeptFooter = (e.Row.FindControl("ddlDeptFooter") as DropDownList);
                    if((ddlDeptFooter != null))
                    {
                        ddlDeptFooter.DataSource = dtbl;
                        ddlDeptFooter.DataTextField = "deptName";
                        ddlDeptFooter.DataValueField = "deptID";
                        ddlDeptFooter.DataBind();
                    }

                }

                if(e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList ddlDeptEdit = (e.Row.FindControl("ddlEditDept") as DropDownList);
                    if ((ddlDeptEdit != null))
                    {
                        ddlDeptEdit.DataSource = dtbl;
                        ddlDeptEdit.DataTextField = "deptName";
                        ddlDeptEdit.DataValueField = "deptID";
                        ddlDeptEdit.DataBind();
                    }
                }
            }

        }

    }
}
    
