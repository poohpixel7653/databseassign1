﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Ass1Homework
{
    public partial class IncOfEmp : System.Web.UI.Page
    {

        string connectionString = @"Data Source=DESKTOP-PJ2H1R3\PUPATSQL;Initial Catalog=ex1;Integrated Security=True";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateGridview();
                SuccessBox.Visible = false;
                ErrorBox.Visible = false;
                PopulateColor();
                

            }

        }


        void PopulateGridview()
        {
            DataTable dtbl = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();

                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT TOP 5 item.itemName, SUM(OrderItem.valueItem) FROM OrderItem INNER JOIN Item ON OrderItem.itemID = Item.itemID GROUP BY item.itemName ORDER BY SUM(OrderItem.valueItem) DESC", sqlCon);
                //SqlDataAdapter da = new SqlDataAdapter();
                //string FindLess10 = "SELECT item.itemName, COUNT(*) FROM Item INNER JOIN OrderItem ON OrderItem.itemID = Item.itemID GROUP BY itemID HAVING AVG(COUNT(itemID))";
                //SqlCommand sqlCmdFind10 = new SqlCommand(FindLess10, sqlCon);
                //sqlCmdFind10.ExecuteNonQuery();
                sqlDa.Fill(dtbl);
                


            }
            if (dtbl.Rows.Count > 0)
            {
                gvSales.DataSource = dtbl;
                gvSales.DataBind();
            }
            else
            {
                dtbl.Rows.Add(dtbl.NewRow());
                gvSales.DataSource = dtbl;
                gvSales.DataBind();
                gvSales.Rows[0].Cells.Clear();
                gvSales.Rows[0].Cells.Add(new TableCell());
                gvSales.Rows[0].Cells[0].ColumnSpan = dtbl.Columns.Count;
                gvSales.Rows[0].Cells[0].Text = "No Data Found ..!";
                gvSales.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            }

        }

        void PopulateColor()
        {
            DataTable dtbl = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                string HotColor = "SELECT TOP 1 itemColor, SUM(valueItem) FROM OrderItem GROUP BY itemColor ORDER BY SUM(valueItem) DESC";
                SqlCommand sqlColor = new SqlCommand(HotColor, sqlCon);
                string colorText = (string)sqlColor.ExecuteScalar();
                lblHotColor.Text = "HOT COLOR : " + colorText;
            }
        }


    }

}
