﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Ass1Homework
{
    public partial class IncDeptAdmin : System.Web.UI.Page
    {

        string connectionString = @"Data Source=DESKTOP-PJ2H1R3\PUPATSQL;Initial Catalog=ex1;Integrated Security=True";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateGridview();
                calculateSum();
                SuccessBox.Visible = false;
                ErrorBox.Visible = false;
            }

        }
        void calculateSum()
        {
            DataTable dt = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("select * from Department", sqlCon);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                gvDepartment.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Left;
                gvDepartment.FooterRow.Cells[2].Text = "Total :" + dt.Compute("Sum(deptSales)", "").ToString(); ;
                //gvDepartment.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                //gvDepartment.FooterRow.Cells[1].Text = dt.Compute("Sum(deptDepartment)", "").ToString();
            }

        }

        void PopulateGridview()
        {
            DataTable dtbl = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT * FROM Department", sqlCon);
                sqlDa.Fill(dtbl);

            }
            if (dtbl.Rows.Count > 0)
            {
                gvDepartment.DataSource = dtbl;
                gvDepartment.DataBind();
            }
            else
            {
                dtbl.Rows.Add(dtbl.NewRow());
                gvDepartment.DataSource = dtbl;
                gvDepartment.DataBind();
                gvDepartment.Rows[0].Cells.Clear();
                gvDepartment.Rows[0].Cells.Add(new TableCell());
                gvDepartment.Rows[0].Cells[0].ColumnSpan = dtbl.Columns.Count;
                gvDepartment.Rows[0].Cells[0].Text = "No Data Found ..!";
                gvDepartment.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            }

        }

    }

}
