﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Ass1Homework
{
    public partial class EmpManage : System.Web.UI.Page
    {
        string connectionString = @"Data Source=DESKTOP-PJ2H1R3\PUPATSQL;Initial Catalog=ex1;Integrated Security=True";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //gvItem.Visible = false;
                gvSales.Visible = false;
                alertBox.Visible = false;
                gvAlert.Visible = false;
                SuccessBox.Visible = false;
                ErrorBox.Visible = false;
                gvItem.Visible = false;
                //itemSearch.Visible = false;
            }

        }
        void PopulateTotal()
        {
            DataTable dtblTotal = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                
                sqlCon.Open();
                
                string deptIDByEmpID = "SELECT deptID FROM Employee WHERE employeeID=@empID";
                SqlCommand sqlCmdDeptID = new SqlCommand(deptIDByEmpID, sqlCon);
                sqlCmdDeptID.Parameters.AddWithValue("@empID", TextBoxShEmp.Text);
                string queryByEmpID = (string)sqlCmdDeptID.ExecuteScalar();

                string totalDept = "SELECT Department.deptName,Sales.price * OrderItem.valueItem FROM Department INNER JOIN Sales ON Sales.deptID = Department.deptID INNER JOIN OrderItem ON OrderItem.itemID = Sales.itemID WHERE Department.deptID=@deptID GROUP BY Department.deptName,Sales.price* OrderItem.valueItem";
                SqlDataAdapter da = new SqlDataAdapter();
                SqlCommand sqlDeptTotal = new SqlCommand(totalDept, sqlCon);
                sqlDeptTotal.Parameters.AddWithValue("@deptID" ,queryByEmpID);
                sqlDeptTotal.ExecuteNonQuery();

                da.SelectCommand = sqlDeptTotal;
                da.Fill(dtblTotal);
                int sum = 0;
                foreach (DataRow dr in dtblTotal.Rows)
                {
                    dynamic value = dr[1].ToString();
                    if (!string.IsNullOrEmpty(value))
                    {
                        sum += Convert.ToInt32(value);
                    }
                }
                lblTotalDept.Text = "Department Total :" + sum.ToString();

            }
        }

        //void calculateSum()
        //{
        //    DataTable dt = new DataTable();
        //    using (SqlConnection sqlCon = new SqlConnection(connectionString))
        //    {
        //        SqlCommand cmd = new SqlCommand("SELECT orderItem.valueItem, sales.price,(orderItem.valueItem * sales.price) FROM Sales INNER JOIN OrderItem ON OrderItem.itemID=Sales.ItemID", sqlCon);
        //        SqlDataAdapter da = new SqlDataAdapter(cmd);
        //        da.Fill(dt);
        //        gvItem0.DataSource = dt;
        //        gvItem0.DataBind();

        //        gvItem0.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Left;
        //        gvItem0.FooterRow.Cells[1].Text = "Total :" + dt.Compute("Sum()", "").ToString();
        //    }

        //}


        void PopulateItemGridview()
        {
            DataTable dtblItem = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                //SqlDataAdapter sqlDaItem = new SqlDataAdapter("SELECT item.itemID,item.itemName,item.color,item.stroge,Sales.price,Department.deptName FROM Item JOIN Sales ON item.itemID=Sales.itemID JOIN Department ON Sales.deptID=Department.deptID WHERE Department.DeptID = 'DEPT03'", sqlCon);
                SqlDataAdapter sqlDaItem = new SqlDataAdapter("SELECT item.itemID,item.itemName,item.color,item.stroge,Sales.price,Department.deptName FROM Item JOIN Sales ON item.itemID=Sales.itemID JOIN Department ON Sales.deptID=Department.deptID ", sqlCon);
                sqlDaItem.Fill(dtblItem);
            }
            if (dtblItem.Rows.Count > 0)
            {
                gvItem.DataSource = dtblItem;
                gvItem.DataBind();
            }
            else
            {
                dtblItem.Rows.Add(dtblItem.NewRow());
                gvItem.DataSource = dtblItem;
                gvItem.DataBind();
                gvItem.Rows[0].Cells.Clear();
                gvItem.Rows[0].Cells.Add(new TableCell());
                gvItem.Rows[0].Cells[0].ColumnSpan = dtblItem.Columns.Count;
                gvItem.Rows[0].Cells[0].Text = "No Data Found ..!";
                gvItem.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            }
        }


        protected void gvItem_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("AddNew"))
                {
                    using (SqlConnection sqlCon = new SqlConnection(connectionString))
                    {


                        sqlCon.Open();
                        string generateID = Generate();
                        int idChangeType = Convert.ToInt32(generateID);
                        idChangeType += 1;

                        string queryItem = "INSERT INTO Item (itemID,itemName,Color,Stroge) VALUES (@itemID,@itemName,@itemColor,@itemStock)";
                        string querySales = "INSERT INTO Sales (price,deptID,itemID) VALUES (@itemPrice,@itemDept,@itemID)";

                        SqlCommand sqlCmdItem = new SqlCommand(queryItem, sqlCon);
                        SqlCommand sqlCmdSales = new SqlCommand(querySales, sqlCon);
                        sqlCmdItem.Parameters.AddWithValue("@itemID", "IM0" + idChangeType.ToString());
                        sqlCmdSales.Parameters.AddWithValue("@itemID", "IM0" + idChangeType.ToString());
                        sqlCmdItem.Parameters.AddWithValue("@itemName", (gvItem.FooterRow.FindControl("txtitemNameFooter") as TextBox).Text.Trim());
                        sqlCmdItem.Parameters.AddWithValue("@itemColor", (gvItem.FooterRow.FindControl("txtColorFooter") as DropDownList).SelectedValue);
                        sqlCmdSales.Parameters.AddWithValue("@itemPrice", (gvItem.FooterRow.FindControl("txtPriceFooter") as TextBox).Text.Trim());
                        sqlCmdItem.Parameters.AddWithValue("@itemDept", (gvItem.FooterRow.FindControl("ddlDeptFooter") as DropDownList).SelectedValue);
                        sqlCmdSales.Parameters.AddWithValue("@itemDept", (gvItem.FooterRow.FindControl("ddlDeptFooter") as DropDownList).SelectedValue);
                        sqlCmdItem.Parameters.AddWithValue("@itemStock", (gvItem.FooterRow.FindControl("txtStockFooter") as TextBox).Text.Trim());
                        sqlCmdItem.ExecuteNonQuery();
                        sqlCmdSales.ExecuteNonQuery();
                        FindItemByDeptID();
                        alertBox.Visible = false;
                        FindLess10();
                        lblSuccessMessage.Text = "New Record Added";
                        lblErrorMessage.Text = "";
                        SuccessBox.Visible = true;
                        SelectDept();


                    }
                }
            }
            catch (Exception ex)
            {

                lblSuccessMessage.Text = "";
                lblErrorMessage.Text = ex.Message;
                ErrorBox.Visible = true;
            }
        }

        protected void gvItem_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvItem.EditIndex = e.NewEditIndex;
            FindItemByDeptID();
            SelectDept();
        }

        protected void gvItem_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvItem.EditIndex = -1;
            FindItemByDeptID();
            SelectDept();

        }

        protected void gvItem_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();

                    string querySales = "UPDATE Sales SET Price=@itemPrice WHERE itemID = @idSales";
                    SqlCommand sqlCmdSales = new SqlCommand(querySales, sqlCon);
                    sqlCmdSales.Parameters.AddWithValue("@itemPrice", (gvItem.Rows[e.RowIndex].FindControl("txtPrice") as TextBox).Text.Trim());
                    sqlCmdSales.Parameters.AddWithValue("@idSales", (gvItem.Rows[e.RowIndex].FindControl("txtItemID") as Label).Text.Trim());
                    sqlCmdSales.ExecuteNonQuery();


                    string queryItem = "UPDATE Item SET itemName=@itemName,color=@itemColor,stroge=@itemStock WHERE ItemID = @idItem";
                    SqlCommand sqlCmdItem = new SqlCommand(queryItem, sqlCon);
                    sqlCmdItem.Parameters.AddWithValue("@itemName", (gvItem.Rows[e.RowIndex].FindControl("txtItemName") as TextBox).Text.Trim());
                    sqlCmdItem.Parameters.AddWithValue("@itemColor", (gvItem.Rows[e.RowIndex].FindControl("txtColor") as DropDownList).SelectedValue);
                    sqlCmdItem.Parameters.AddWithValue("@itemStock", (gvItem.Rows[e.RowIndex].FindControl("txtStock") as TextBox).Text.Trim());
                    sqlCmdItem.Parameters.AddWithValue("@idItem", (gvItem.Rows[e.RowIndex].FindControl("txtItemID") as Label).Text.Trim());
                    sqlCmdItem.ExecuteNonQuery();
                    gvItem.EditIndex = -1;
                    FindLess10();
                    lblSuccessMessage.Text = "Selected Record Update";
                    lblErrorMessage.Text = "";
                    SuccessBox.Visible = true;
                    FindItemByDeptID();
                    SelectDept();
                }   

            }
            catch (Exception ex)
            {

                lblSuccessMessage.Text = "";
                lblErrorMessage.Text = ex.Message;
                ErrorBox.Visible = true;
            }
        }

        void FindLess10()
        {
            DataTable dt = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                string EmpID = "SELECT employeeID FROM Employee WHERE employeeID=@empIDByID";
                SqlCommand sqlCmd = new SqlCommand(EmpID, sqlCon);
                sqlCmd.Parameters.AddWithValue("@empIDByID", TextBoxShEmp.Text);
                string EmpIDbyID = (string)sqlCmd.ExecuteScalar();

                if (TextBoxShEmp.Text != EmpIDbyID)
                {
                    lblErrorMessage.Text = "PLEASE ENTER EmployeeID AGAIN";
                    ErrorBox.Visible = true;
                    PopulateItemGridview();
                }
                else
                {
                    string deptIDByEmpID = "SELECT deptID FROM Employee WHERE employeeID=@empID";
                    SqlCommand sqlCmdDeptID = new SqlCommand(deptIDByEmpID, sqlCon);
                    sqlCmdDeptID.Parameters.AddWithValue("@empID", TextBoxShEmp.Text);
                    string queryByEmpID = (string)sqlCmdDeptID.ExecuteScalar();

                    SqlDataAdapter da = new SqlDataAdapter();
                    string FindLess10 = "SELECT item.stroge, item.itemName FROM Item JOIN Sales ON item.itemID = Sales.itemID JOIN Department ON Sales.deptID = Department.deptID WHERE Department.deptID = @deptID AND item.stroge <= 10";
                    SqlCommand sqlCmdFind10 = new SqlCommand(FindLess10, sqlCon);
                    sqlCmdFind10.Parameters.AddWithValue("@deptID", queryByEmpID);
                    sqlCmdFind10.ExecuteNonQuery();

                    da.SelectCommand = sqlCmdFind10;
                    da.Fill(dt);
                    gvAlert.DataSource = dt;
                    gvAlert.DataBind();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (GridViewRow row in gvAlert.Rows)
                        {
                            Label lblItem10 = row.FindControl("lblArItem") as Label;
                            Label lblItem10item = row.FindControl("lblArStock") as Label;
                            Label span = new Label();
                            span.Text = "PRODUCT <strong>" + lblItem10.Text + "</strong> HAS <strong>" + lblItem10item.Text + "</strong> ITEM <br/>";
                            span.CssClass = "nonExpense";
                            alertBox.Controls.Add(span);
                        }
                        alertBox.Visible = true;
                    }
                }

            }

        }


        private void SelectDept()
        {

            DataTable dtbl = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                string EmpID = "SELECT employeeID FROM Employee WHERE employeeID=@empIDByID";
                SqlCommand sqlCmd = new SqlCommand(EmpID, sqlCon);
                sqlCmd.Parameters.AddWithValue("@empIDByID", TextBoxShEmp.Text);
                string EmpIDbyID = (string)sqlCmd.ExecuteScalar();

                if (TextBoxShEmp.Text != EmpIDbyID)
                {
                    lblErrorMessage.Text = "PLEASE ENTER EmployeeID AGAIN";
                    ErrorBox.Visible = true;
                    PopulateItemGridview();
                }
                else
                {
                    string deptIDByEmpID = "SELECT deptID FROM Employee WHERE employeeID=@empID";
                    SqlCommand sqlCmdDeptID = new SqlCommand(deptIDByEmpID, sqlCon);
                    sqlCmdDeptID.Parameters.AddWithValue("@empID", TextBoxShEmp.Text);
                    string queryByEmpID = (string)sqlCmdDeptID.ExecuteScalar();

                    string query = "SELECT * FROM Department WHERE deptID=@deptID";
                    SqlDataAdapter sqlDa = new SqlDataAdapter();
                    SqlCommand sqlCmdDept = new SqlCommand(query, sqlCon);
                    sqlCmdDept.Parameters.AddWithValue("deptID", queryByEmpID);
                    sqlDa.SelectCommand = sqlCmdDept;
                    sqlDa.Fill(dtbl);

                    DropDownList ddlDeptFooter = (gvItem.FooterRow.FindControl("ddlDeptFooter") as DropDownList);
                    if ((ddlDeptFooter != null))
                    {
                        ddlDeptFooter.DataSource = dtbl;
                        ddlDeptFooter.DataTextField = "deptName";
                        ddlDeptFooter.DataValueField = "deptID";
                        ddlDeptFooter.DataBind();
                    }

                }



            }

        }

        private string Generate()
        {
            DataTable dtbl = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                string query = "SELECT itemID FROM Item ORDER BY itemID";
                SqlDataAdapter sqlDa = new SqlDataAdapter(query, sqlCon);
                sqlDa.Fill(dtbl);


                if (dtbl.Rows.Count <= 0)
                {
                    return "0";
                }
                else
                {
                    return (dtbl.Rows.Count + 1).ToString();
                }


            }

        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            FindLess10();
            //PopulateItemGridview();
            FindItemByDeptID();
            SelectDept();
        }

        private void FindItemByDeptID()
        {
            DataTable dtbl = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();

                string EmpID = "SELECT employeeID FROM Employee WHERE employeeID=@empIDByID";
                SqlCommand sqlCmd = new SqlCommand(EmpID, sqlCon);
                sqlCmd.Parameters.AddWithValue("@empIDByID", TextBoxShEmp.Text);
                string EmpIDbyID = (string)sqlCmd.ExecuteScalar();

                if (TextBoxShEmp.Text != EmpIDbyID)
                {
                    lblErrorMessage.Text = "PLEASE ENTER EmployeeID AGAIN";
                    ErrorBox.Visible = true;
                    PopulateItemGridview();
                }
                else
                {
                    string deptIDByEmpID = "SELECT deptID FROM Employee WHERE employeeID=@empID";
                    SqlCommand sqlCmdDeptID = new SqlCommand(deptIDByEmpID, sqlCon);
                    sqlCmdDeptID.Parameters.AddWithValue("@empID", TextBoxShEmp.Text);
                    string queryByEmpID = (string)sqlCmdDeptID.ExecuteScalar();

                    SqlDataAdapter da = new SqlDataAdapter();
                    string ItemByDept = "SELECT item.itemID,item.itemName,item.color,item.stroge,Sales.price,Department.deptName FROM Item JOIN Sales ON item.itemID=Sales.itemID JOIN Department ON Sales.deptID=Department.deptID WHERE Department.DeptID = @deptID";
                    SqlCommand sqlCmdItemID = new SqlCommand(ItemByDept, sqlCon);
                    sqlCmdItemID.Parameters.AddWithValue("@deptID", queryByEmpID);
                    sqlCmdDeptID.ExecuteNonQuery();

                    da.SelectCommand = sqlCmdItemID;
                    da.Fill(dtbl);
                    gvItem.Visible = true;
                    PopulateTotal();
                    if (dtbl.Rows.Count > 0)
                    {
                        gvItem.DataSource = dtbl;
                        gvItem.DataBind();
                    }
                    else
                    {
                        dtbl.Rows.Add(dtbl.NewRow());
                        gvItem.DataSource = dtbl;
                        gvItem.DataBind();
                        gvItem.Rows[0].Cells.Clear();
                        gvItem.Rows[0].Cells.Add(new TableCell());
                        gvItem.Rows[0].Cells[0].ColumnSpan = dtbl.Columns.Count;
                        gvItem.Rows[0].Cells[0].Text = "No Data Found ..!";
                        gvItem.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
                    }
                    
                }
            }
        }


        protected void gvItem_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {

                    sqlCon.Open();

                    string queryOrder = "DELETE FROM OrderItem WHERE itemID=@idOrder";
                    SqlCommand sqlCmdOrder = new SqlCommand(queryOrder, sqlCon);
                    sqlCmdOrder.Parameters.AddWithValue("@idOrder", (gvItem.Rows[e.RowIndex].FindControl("txtItemID") as Label).Text.Trim());
                    sqlCmdOrder.ExecuteNonQuery();

                    string queryCart = "DELETE FROM Cart WHERE itemID=@idCart";
                    SqlCommand sqlCmdCart = new SqlCommand(queryCart, sqlCon);
                    sqlCmdCart.Parameters.AddWithValue("@idCart", (gvItem.Rows[e.RowIndex].FindControl("txtItemID") as Label).Text.Trim());
                    sqlCmdCart.ExecuteNonQuery();

                    string querySales = "DELETE FROM Sales WHERE itemID=@idSales";
                    SqlCommand sqlCmdSales = new SqlCommand(querySales, sqlCon);
                    sqlCmdSales.Parameters.AddWithValue("@idSales", (gvItem.Rows[e.RowIndex].FindControl("txtItemID") as Label).Text.Trim());
                    sqlCmdSales.ExecuteNonQuery();

                    string queryItem = "DELETE FROM Item WHERE itemID=@idItem";
                    SqlCommand sqlCmdItem = new SqlCommand(queryItem, sqlCon);
                    sqlCmdItem.Parameters.AddWithValue("@idItem", (gvItem.Rows[e.RowIndex].FindControl("txtItemID") as Label).Text.Trim());
                    sqlCmdItem.ExecuteNonQuery();
                    alertBox.Visible = false;
                    FindLess10();
                    FindItemByDeptID();
                    lblSuccessMessage.Text = "Selected Record Deleted";
                    lblErrorMessage.Text = "";
                    SuccessBox.Visible = true;
                    SelectDept();
                }

            }
            catch (Exception ex)
            {

                lblSuccessMessage.Text = "";
                lblErrorMessage.Text = ex.Message;
                ErrorBox.Visible = true;
            }
        }

    }
}
